﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;

namespace Card_less7
{

    public partial class Form1 : Form
    {

        Image[] images = new Image[52];
        private static TcpClient client = new TcpClient();
        private static IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
        private static NetworkStream clientStream;
        private static int ScoringY=0;
        private static int ScoringP = 0;



        public Form1()
        {
            InitializeComponent();
            client.Connect(serverEndPoint);
            clientStream = client.GetStream();
            chkRadiation_CheckedChanged();
            Thread myThread = new Thread(getMessageServer);
            myThread.Start();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        string card2="";
        bool sa = false;
        bool choes = false;
        private void getMessageServer()
        {
            Thread.Sleep(2000);
            //whiet foe messeage from Server
            while (true)
            {
                if(sa && choes)
                {
                   
                    GetNumImage();
                }

                byte[] buffer = new byte[4];
                int bytesRead = clientStream.Read(buffer, 0, 4);
                string input = new ASCIIEncoding().GetString(buffer);
                if(input == "0000")
                {
                    Invoke((MethodInvoker)delegate { GenerateCards(); });
                }
                else
                {
                    card2 = input;
                    sa = true;
                    if(card2[0] == '2')
                    {
                        this.Close();
                    }
                }
            }
        }

        //sent meesage to client
        private void SentToServer(string card)
        {

            byte[] buffer = new ASCIIEncoding().GetBytes(card);
            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();
        }
        int card=0;
        //Changes the legality of the cards
        private void GetNumImage()
        {
            sa = false;
            if (card2[0] == '1')
            {
                int number = 0;
                if (card2[1] == '0')
                {
                    number = card2[2] - '0';
                    if (card2[3] == 'C')
                    {
                        number = number * 4 - 4;
                    }
                    if (card2[3] == 'D')
                    {
                        number = number * 4 - 3;

                    }
                    if (card2[3] == 'H')
                    {
                        number = number * 4 - 2;
                    }
                    if (card2[3] == 'S')
                    {
                        number = number * 4 - 1;
                    }
                }
                else
                {
                    number = 10 + card2[2] - '0';
                    if (card2[3] == 'C')
                    {
                        number = number * 4 - 4;
                    }
                    if (card2[3] == 'D')
                    {
                        number = number * 4 - 3;

                    }
                    if (card2[3] == 'H')
                    {
                        number = number * 4 - 2;
                    }
                    if (card2[3] == 'S')
                    {
                        number = number * 4 - 1;
                    }
                }
                if(number > card && card!= 0)
                {
                    ScoringY--;
                    ScoringP++;
                    
                }
                else if(card < number && card != 0)
                {
                    ScoringY++;
                    ScoringP--;

                }

                back_blue.Image = images[number];

            }
        }


        //add the card In ascending order And the legality of the colors
        private void chkRadiation_CheckedChanged()
        {

            int k = 0;
            images[k] = (Image)Properties.Resources.ResourceManager.GetObject( "ace"+ "_of_clubs");
            k++;
            images[k] = (Image)Properties.Resources.ResourceManager.GetObject("ace" + "_of_diamonds" );
            k++;
            images[k] = (Image)Properties.Resources.ResourceManager.GetObject("ace" + "_of_hearts" );
            k++;
            images[k] = (Image)Properties.Resources.ResourceManager.GetObject("ace" + "_of_spades2");
            k++;

            
            for (int i = 2; i<11;i++)
            {

                images[k] = (Image)Properties.Resources.ResourceManager.GetObject("_"+i + "_of_clubs");
                k++;
                images[k] = (Image)Properties.Resources.ResourceManager.GetObject("_"+i + "_of_diamonds");
                k++;
                images[k] = (Image)Properties.Resources.ResourceManager.GetObject("_"+i + "_of_hearts");
                k++;
                images[k] = (Image)Properties.Resources.ResourceManager.GetObject("_"+i + "_of_spades");
                k++;
                
            }
            string a = "jack";//chane the name card

            for (int j=1; j<3;j++)
            {
               
                 if(j ==2)
                {
                    a = "queen";
                }
                else if(j==3)
                {
                    a = "king";
                }
                images[k] = (Image)Properties.Resources.ResourceManager.GetObject(a + "_of_clubs"+ "2");
                k++;
                images[k] = (Image)Properties.Resources.ResourceManager.GetObject(a + "_of_diamonds"+ "2");
                k++;
                images[k] = (Image)Properties.Resources.ResourceManager.GetObject(a + "_of_hearts"+ "2");
                k++;
                images[k] = (Image)Properties.Resources.ResourceManager.GetObject(a + "_of_spades"+"2");
                k++; //the k in end is 51 the len of arrey

            }


        }
        PictureBox currentPic2;//temp

        private void GenerateCards()
        {

            Point nextLocation = new Point(x: Forfeit.Location.X - Forfeit.Location.X +10 , y: Forfeit.Location.Y + Forfeit.Size.Height + 200);
            Random rnd = new Random();//get random

            for (int i = 0; i < 10; i++)
            {
                

                // create the image object itself
                PictureBox currentPic = new PictureBox();
                currentPic.Name = "picDynamic" + i; // in our case, this is the only property that changes between the different images
                currentPic.Image = global::Card_less7.Properties.Resources.card_back_red;
                currentPic.Location = nextLocation;
                currentPic.Size = new System.Drawing.Size(100, 114);
                currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

                // assign an event to it
                
                currentPic.Click += delegate (object sender1, EventArgs e1)
                {
                    int num1 = rnd.Next(1, 14); // 1-13 // the num
                    int num2 = rnd.Next(1, 5); //the value
                    string currentIndex = currentPic.Name.Substring(currentPic.Name.Length - 1);
                    // use the delegate's params in order to remove the specific image which was clicked
                    if (choes)
                    {
                        currentPic2.Image = global::Card_less7.Properties.Resources.card_back_red;
                        back_blue.Image = global::Card_less7.Properties.Resources.card_back_blue;
                        
                    }
                    currentPic.Image = images[num1 * 4 - num2];//Changes the legality of the cards
                    currentPic2 = currentPic;

                    string cardCode ="";
                    if (num1 / 10 == 0)
                    {
                        cardCode = "10" + num1;
                        if (num2 == 1)
                        {
                            cardCode += "S";
                        }
                        if (num2 == 2)
                        {
                            cardCode += "H";
                        }
                        if (num2 == 3)
                        {
                            cardCode += "D";
                        }
                        if (num2 == 4)
                        {
                            cardCode += "C";
                        }

                    }
                    else
                    {
                        cardCode = "1" + num1;
                        if (num2 == 1)
                        {
                            cardCode += "S";
                        }
                        if (num2 == 2)
                        {
                            cardCode += "H";
                        }
                        if (num2 == 3)
                        {
                            cardCode += "D";
                        }
                        if (num2 == 4)
                        {
                            cardCode += "C";
                        }
                    }
                    
                    SentToServer(cardCode);

                    choes = true;

                   if (sa)
                   {
                      card = num1;
                      GetNumImage();
                      ScoringYou.Text = "Scoring You: " + ScoringYou.ToString();
                      ScoringPlayer.Text = "Scoring Player: " + ScoringP.ToString();
                    }


                    GenerateCards();


                };


                Controls.Add(currentPic);
                nextLocation.X += currentPic.Size.Width + 10;

            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        //exit fron the server and print the sourch
        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            MessageBox.Show(ScoringYou.Text.ToString() +" and " + ScoringPlayer.Text.ToString());

            this.Close();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
