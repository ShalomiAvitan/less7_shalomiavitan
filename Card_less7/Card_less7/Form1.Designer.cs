﻿namespace Card_less7
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Forfeit = new System.Windows.Forms.Button();
            this.back_blue = new System.Windows.Forms.PictureBox();
            this.ScoringPlayer = new System.Windows.Forms.Label();
            this.ScoringYou = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.back_blue)).BeginInit();
            this.SuspendLayout();
            // 
            // Forfeit
            // 
            this.Forfeit.Location = new System.Drawing.Point(540, 29);
            this.Forfeit.Name = "Forfeit";
            this.Forfeit.Size = new System.Drawing.Size(83, 23);
            this.Forfeit.TabIndex = 0;
            this.Forfeit.Text = "Forfeit";
            this.Forfeit.UseVisualStyleBackColor = true;
            // 
            // back_blue
            // 
            this.back_blue.Image = global::Card_less7.Properties.Resources.card_back_blue;
            this.back_blue.Location = new System.Drawing.Point(475, 71);
            this.back_blue.Name = "back_blue";
            this.back_blue.Size = new System.Drawing.Size(212, 135);
            this.back_blue.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.back_blue.TabIndex = 1;
            this.back_blue.TabStop = false;
            this.back_blue.Click += new System.EventHandler(this.pictureBox1_Click_1);
            // 
            // ScoringPlayer
            // 
            this.ScoringPlayer.AutoSize = true;
            this.ScoringPlayer.Location = new System.Drawing.Point(50, 29);
            this.ScoringPlayer.Name = "ScoringPlayer";
            this.ScoringPlayer.Size = new System.Drawing.Size(87, 13);
            this.ScoringPlayer.TabIndex = 2;
            this.ScoringPlayer.Text = "Scoring Player: 0";
            // 
            // ScoringYou
            // 
            this.ScoringYou.AutoSize = true;
            this.ScoringYou.Location = new System.Drawing.Point(1014, 29);
            this.ScoringYou.Name = "ScoringYou";
            this.ScoringYou.Size = new System.Drawing.Size(77, 13);
            this.ScoringYou.TabIndex = 3;
            this.ScoringYou.Text = "Scoring You: 0";
            this.ScoringYou.Click += new System.EventHandler(this.label2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1125, 491);
            this.Controls.Add(this.ScoringYou);
            this.Controls.Add(this.ScoringPlayer);
            this.Controls.Add(this.back_blue);
            this.Controls.Add(this.Forfeit);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.back_blue)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Forfeit;
        private System.Windows.Forms.PictureBox back_blue;
        private System.Windows.Forms.Label ScoringPlayer;
        private System.Windows.Forms.Label ScoringYou;
    }
}

